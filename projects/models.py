from django.db import models

from django.contrib.auth.models import User


# Creating a model for projects
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(User, related_name="projects")

    def __str__(self):
        return self.name
