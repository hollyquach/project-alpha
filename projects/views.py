# from django.shortcuts import render

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.shortcuts import redirect

from .models import Project


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "new.html"
    fields = ["name", "description", "members"]

    def form_valid(self, form):
        new = form.save(commit=False)
        new.save()
        form.save_m2m()
        return redirect("show_project", pk=new.id)
